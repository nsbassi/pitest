
package biztalk.matmstr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="MaterialMasterinfo"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="PlantID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="IndustrySector" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="MaterialType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="MaterialName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="BasicDataText" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="OldMaterialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ProductInspectionMemo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="PharmacopeialName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="BasicMaterial" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Document" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Class" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="BatchManagement" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="PurchaseOrderText" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="MinimumRemainingShelfLife" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="TotalShelfLife" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="PeriodIndicatorforSLED" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="InspectionInterval" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="GRProcessingTime" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="QMProcurementActiveIndicator" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="CertificateType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="MaterialGroup" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="BasicUnitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="BlockedStatus" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="AvailabilityCheck" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ProfitCentre" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="PurchasingGroup" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="MRPType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="MRPController" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Lotsize" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="PlannedDeliveryTime" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="QMControlKey" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="SourceList" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Individualcoll" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="StorageConditions" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="TempConditions" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="IndustrystandardDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="MaterialCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="Assay" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="FixedLotSize" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="LoadingGroup" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="TransportationGroup" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="VarianceKey" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="ClassType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "materialMasterinfo"
})
@XmlRootElement(name = "MaterialMaster")
public class MaterialMaster {

    @XmlElement(name = "MaterialMasterinfo", required = true)
    protected MaterialMaster.MaterialMasterinfo materialMasterinfo;

    /**
     * Gets the value of the materialMasterinfo property.
     * 
     * @return
     *     possible object is
     *     {@link MaterialMaster.MaterialMasterinfo }
     *     
     */
    public MaterialMaster.MaterialMasterinfo getMaterialMasterinfo() {
        return materialMasterinfo;
    }

    /**
     * Sets the value of the materialMasterinfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaterialMaster.MaterialMasterinfo }
     *     
     */
    public void setMaterialMasterinfo(MaterialMaster.MaterialMasterinfo value) {
        this.materialMasterinfo = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="PlantID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="IndustrySector" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="MaterialType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="MaterialName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="BasicDataText" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="OldMaterialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ProductInspectionMemo" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="PharmacopeialName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="BasicMaterial" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Document" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Class" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="BatchManagement" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="PurchaseOrderText" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="MinimumRemainingShelfLife" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="TotalShelfLife" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="PeriodIndicatorforSLED" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="InspectionInterval" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="GRProcessingTime" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="QMProcurementActiveIndicator" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="CertificateType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="MaterialGroup" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="BasicUnitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="BlockedStatus" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="AvailabilityCheck" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ProfitCentre" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="PurchasingGroup" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="MRPType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="MRPController" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Lotsize" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="PlannedDeliveryTime" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="QMControlKey" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="SourceList" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Individualcoll" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="StorageConditions" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="TempConditions" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="IndustrystandardDescription" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="MaterialCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="Assay" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="FixedLotSize" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="LoadingGroup" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="TransportationGroup" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="VarianceKey" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="ClassType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "plantID",
        "industrySector",
        "materialType",
        "materialName",
        "basicDataText",
        "oldMaterialNumber",
        "productInspectionMemo",
        "pharmacopeialName",
        "basicMaterial",
        "document",
        "clazz",
        "batchManagement",
        "purchaseOrderText",
        "minimumRemainingShelfLife",
        "totalShelfLife",
        "periodIndicatorforSLED",
        "inspectionInterval",
        "grProcessingTime",
        "qmProcurementActiveIndicator",
        "certificateType",
        "materialGroup",
        "basicUnitOfMeasure",
        "blockedStatus",
        "availabilityCheck",
        "profitCentre",
        "purchasingGroup",
        "mrpType",
        "mrpController",
        "lotsize",
        "plannedDeliveryTime",
        "qmControlKey",
        "sourceList",
        "individualcoll",
        "storageConditions",
        "tempConditions",
        "industrystandardDescription",
        "materialCode",
        "assay",
        "fixedLotSize",
        "loadingGroup",
        "transportationGroup",
        "varianceKey",
        "classType"
    })
    public static class MaterialMasterinfo {

        @XmlElement(name = "PlantID", required = true)
        protected String plantID;
        @XmlElement(name = "IndustrySector", required = true)
        protected String industrySector;
        @XmlElement(name = "MaterialType", required = true)
        protected String materialType;
        @XmlElement(name = "MaterialName", required = true)
        protected String materialName;
        @XmlElement(name = "BasicDataText", required = true)
        protected String basicDataText;
        @XmlElement(name = "OldMaterialNumber", required = true)
        protected String oldMaterialNumber;
        @XmlElement(name = "ProductInspectionMemo", required = true)
        protected String productInspectionMemo;
        @XmlElement(name = "PharmacopeialName", required = true)
        protected String pharmacopeialName;
        @XmlElement(name = "BasicMaterial", required = true)
        protected String basicMaterial;
        @XmlElement(name = "Document", required = true)
        protected String document;
        @XmlElement(name = "Class", required = true)
        protected String clazz;
        @XmlElement(name = "BatchManagement", required = true)
        protected String batchManagement;
        @XmlElement(name = "PurchaseOrderText", required = true)
        protected String purchaseOrderText;
        @XmlElement(name = "MinimumRemainingShelfLife", required = true)
        protected String minimumRemainingShelfLife;
        @XmlElement(name = "TotalShelfLife", required = true)
        protected String totalShelfLife;
        @XmlElement(name = "PeriodIndicatorforSLED", required = true)
        protected String periodIndicatorforSLED;
        @XmlElement(name = "InspectionInterval", required = true)
        protected String inspectionInterval;
        @XmlElement(name = "GRProcessingTime", required = true)
        protected String grProcessingTime;
        @XmlElement(name = "QMProcurementActiveIndicator", required = true)
        protected String qmProcurementActiveIndicator;
        @XmlElement(name = "CertificateType", required = true)
        protected String certificateType;
        @XmlElement(name = "MaterialGroup", required = true)
        protected String materialGroup;
        @XmlElement(name = "BasicUnitOfMeasure", required = true)
        protected String basicUnitOfMeasure;
        @XmlElement(name = "BlockedStatus", required = true)
        protected String blockedStatus;
        @XmlElement(name = "AvailabilityCheck", required = true)
        protected String availabilityCheck;
        @XmlElement(name = "ProfitCentre", required = true)
        protected String profitCentre;
        @XmlElement(name = "PurchasingGroup", required = true)
        protected String purchasingGroup;
        @XmlElement(name = "MRPType", required = true)
        protected String mrpType;
        @XmlElement(name = "MRPController", required = true)
        protected String mrpController;
        @XmlElement(name = "Lotsize", required = true)
        protected String lotsize;
        @XmlElement(name = "PlannedDeliveryTime", required = true)
        protected String plannedDeliveryTime;
        @XmlElement(name = "QMControlKey", required = true)
        protected String qmControlKey;
        @XmlElement(name = "SourceList", required = true)
        protected String sourceList;
        @XmlElement(name = "Individualcoll", required = true)
        protected String individualcoll;
        @XmlElement(name = "StorageConditions", required = true)
        protected String storageConditions;
        @XmlElement(name = "TempConditions", required = true)
        protected String tempConditions;
        @XmlElement(name = "IndustrystandardDescription", required = true)
        protected String industrystandardDescription;
        @XmlElement(name = "MaterialCode", required = true)
        protected String materialCode;
        @XmlElement(name = "Assay", required = true)
        protected String assay;
        @XmlElement(name = "FixedLotSize", required = true)
        protected String fixedLotSize;
        @XmlElement(name = "LoadingGroup", required = true)
        protected String loadingGroup;
        @XmlElement(name = "TransportationGroup", required = true)
        protected String transportationGroup;
        @XmlElement(name = "VarianceKey", required = true)
        protected String varianceKey;
        @XmlElement(name = "ClassType", required = true)
        protected String classType;

        /**
         * Gets the value of the plantID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPlantID() {
            return plantID;
        }

        /**
         * Sets the value of the plantID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPlantID(String value) {
            this.plantID = value;
        }

        /**
         * Gets the value of the industrySector property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIndustrySector() {
            return industrySector;
        }

        /**
         * Sets the value of the industrySector property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIndustrySector(String value) {
            this.industrySector = value;
        }

        /**
         * Gets the value of the materialType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMaterialType() {
            return materialType;
        }

        /**
         * Sets the value of the materialType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMaterialType(String value) {
            this.materialType = value;
        }

        /**
         * Gets the value of the materialName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMaterialName() {
            return materialName;
        }

        /**
         * Sets the value of the materialName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMaterialName(String value) {
            this.materialName = value;
        }

        /**
         * Gets the value of the basicDataText property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBasicDataText() {
            return basicDataText;
        }

        /**
         * Sets the value of the basicDataText property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBasicDataText(String value) {
            this.basicDataText = value;
        }

        /**
         * Gets the value of the oldMaterialNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOldMaterialNumber() {
            return oldMaterialNumber;
        }

        /**
         * Sets the value of the oldMaterialNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOldMaterialNumber(String value) {
            this.oldMaterialNumber = value;
        }

        /**
         * Gets the value of the productInspectionMemo property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProductInspectionMemo() {
            return productInspectionMemo;
        }

        /**
         * Sets the value of the productInspectionMemo property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProductInspectionMemo(String value) {
            this.productInspectionMemo = value;
        }

        /**
         * Gets the value of the pharmacopeialName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPharmacopeialName() {
            return pharmacopeialName;
        }

        /**
         * Sets the value of the pharmacopeialName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPharmacopeialName(String value) {
            this.pharmacopeialName = value;
        }

        /**
         * Gets the value of the basicMaterial property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBasicMaterial() {
            return basicMaterial;
        }

        /**
         * Sets the value of the basicMaterial property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBasicMaterial(String value) {
            this.basicMaterial = value;
        }

        /**
         * Gets the value of the document property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDocument() {
            return document;
        }

        /**
         * Sets the value of the document property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDocument(String value) {
            this.document = value;
        }

        /**
         * Gets the value of the clazz property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClazz() {
            return clazz;
        }

        /**
         * Sets the value of the clazz property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClazz(String value) {
            this.clazz = value;
        }

        /**
         * Gets the value of the batchManagement property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBatchManagement() {
            return batchManagement;
        }

        /**
         * Sets the value of the batchManagement property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBatchManagement(String value) {
            this.batchManagement = value;
        }

        /**
         * Gets the value of the purchaseOrderText property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPurchaseOrderText() {
            return purchaseOrderText;
        }

        /**
         * Sets the value of the purchaseOrderText property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPurchaseOrderText(String value) {
            this.purchaseOrderText = value;
        }

        /**
         * Gets the value of the minimumRemainingShelfLife property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMinimumRemainingShelfLife() {
            return minimumRemainingShelfLife;
        }

        /**
         * Sets the value of the minimumRemainingShelfLife property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMinimumRemainingShelfLife(String value) {
            this.minimumRemainingShelfLife = value;
        }

        /**
         * Gets the value of the totalShelfLife property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTotalShelfLife() {
            return totalShelfLife;
        }

        /**
         * Sets the value of the totalShelfLife property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTotalShelfLife(String value) {
            this.totalShelfLife = value;
        }

        /**
         * Gets the value of the periodIndicatorforSLED property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPeriodIndicatorforSLED() {
            return periodIndicatorforSLED;
        }

        /**
         * Sets the value of the periodIndicatorforSLED property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPeriodIndicatorforSLED(String value) {
            this.periodIndicatorforSLED = value;
        }

        /**
         * Gets the value of the inspectionInterval property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInspectionInterval() {
            return inspectionInterval;
        }

        /**
         * Sets the value of the inspectionInterval property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInspectionInterval(String value) {
            this.inspectionInterval = value;
        }

        /**
         * Gets the value of the grProcessingTime property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGRProcessingTime() {
            return grProcessingTime;
        }

        /**
         * Sets the value of the grProcessingTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGRProcessingTime(String value) {
            this.grProcessingTime = value;
        }

        /**
         * Gets the value of the qmProcurementActiveIndicator property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQMProcurementActiveIndicator() {
            return qmProcurementActiveIndicator;
        }

        /**
         * Sets the value of the qmProcurementActiveIndicator property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQMProcurementActiveIndicator(String value) {
            this.qmProcurementActiveIndicator = value;
        }

        /**
         * Gets the value of the certificateType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCertificateType() {
            return certificateType;
        }

        /**
         * Sets the value of the certificateType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCertificateType(String value) {
            this.certificateType = value;
        }

        /**
         * Gets the value of the materialGroup property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMaterialGroup() {
            return materialGroup;
        }

        /**
         * Sets the value of the materialGroup property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMaterialGroup(String value) {
            this.materialGroup = value;
        }

        /**
         * Gets the value of the basicUnitOfMeasure property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBasicUnitOfMeasure() {
            return basicUnitOfMeasure;
        }

        /**
         * Sets the value of the basicUnitOfMeasure property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBasicUnitOfMeasure(String value) {
            this.basicUnitOfMeasure = value;
        }

        /**
         * Gets the value of the blockedStatus property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBlockedStatus() {
            return blockedStatus;
        }

        /**
         * Sets the value of the blockedStatus property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBlockedStatus(String value) {
            this.blockedStatus = value;
        }

        /**
         * Gets the value of the availabilityCheck property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAvailabilityCheck() {
            return availabilityCheck;
        }

        /**
         * Sets the value of the availabilityCheck property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAvailabilityCheck(String value) {
            this.availabilityCheck = value;
        }

        /**
         * Gets the value of the profitCentre property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProfitCentre() {
            return profitCentre;
        }

        /**
         * Sets the value of the profitCentre property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProfitCentre(String value) {
            this.profitCentre = value;
        }

        /**
         * Gets the value of the purchasingGroup property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPurchasingGroup() {
            return purchasingGroup;
        }

        /**
         * Sets the value of the purchasingGroup property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPurchasingGroup(String value) {
            this.purchasingGroup = value;
        }

        /**
         * Gets the value of the mrpType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMRPType() {
            return mrpType;
        }

        /**
         * Sets the value of the mrpType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMRPType(String value) {
            this.mrpType = value;
        }

        /**
         * Gets the value of the mrpController property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMRPController() {
            return mrpController;
        }

        /**
         * Sets the value of the mrpController property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMRPController(String value) {
            this.mrpController = value;
        }

        /**
         * Gets the value of the lotsize property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLotsize() {
            return lotsize;
        }

        /**
         * Sets the value of the lotsize property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLotsize(String value) {
            this.lotsize = value;
        }

        /**
         * Gets the value of the plannedDeliveryTime property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPlannedDeliveryTime() {
            return plannedDeliveryTime;
        }

        /**
         * Sets the value of the plannedDeliveryTime property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPlannedDeliveryTime(String value) {
            this.plannedDeliveryTime = value;
        }

        /**
         * Gets the value of the qmControlKey property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQMControlKey() {
            return qmControlKey;
        }

        /**
         * Sets the value of the qmControlKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQMControlKey(String value) {
            this.qmControlKey = value;
        }

        /**
         * Gets the value of the sourceList property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSourceList() {
            return sourceList;
        }

        /**
         * Sets the value of the sourceList property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSourceList(String value) {
            this.sourceList = value;
        }

        /**
         * Gets the value of the individualcoll property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIndividualcoll() {
            return individualcoll;
        }

        /**
         * Sets the value of the individualcoll property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIndividualcoll(String value) {
            this.individualcoll = value;
        }

        /**
         * Gets the value of the storageConditions property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStorageConditions() {
            return storageConditions;
        }

        /**
         * Sets the value of the storageConditions property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStorageConditions(String value) {
            this.storageConditions = value;
        }

        /**
         * Gets the value of the tempConditions property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTempConditions() {
            return tempConditions;
        }

        /**
         * Sets the value of the tempConditions property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTempConditions(String value) {
            this.tempConditions = value;
        }

        /**
         * Gets the value of the industrystandardDescription property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIndustrystandardDescription() {
            return industrystandardDescription;
        }

        /**
         * Sets the value of the industrystandardDescription property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIndustrystandardDescription(String value) {
            this.industrystandardDescription = value;
        }

        /**
         * Gets the value of the materialCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMaterialCode() {
            return materialCode;
        }

        /**
         * Sets the value of the materialCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMaterialCode(String value) {
            this.materialCode = value;
        }

        /**
         * Gets the value of the assay property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAssay() {
            return assay;
        }

        /**
         * Sets the value of the assay property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAssay(String value) {
            this.assay = value;
        }

        /**
         * Gets the value of the fixedLotSize property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFixedLotSize() {
            return fixedLotSize;
        }

        /**
         * Sets the value of the fixedLotSize property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFixedLotSize(String value) {
            this.fixedLotSize = value;
        }

        /**
         * Gets the value of the loadingGroup property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLoadingGroup() {
            return loadingGroup;
        }

        /**
         * Sets the value of the loadingGroup property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLoadingGroup(String value) {
            this.loadingGroup = value;
        }

        /**
         * Gets the value of the transportationGroup property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTransportationGroup() {
            return transportationGroup;
        }

        /**
         * Sets the value of the transportationGroup property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTransportationGroup(String value) {
            this.transportationGroup = value;
        }

        /**
         * Gets the value of the varianceKey property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVarianceKey() {
            return varianceKey;
        }

        /**
         * Sets the value of the varianceKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVarianceKey(String value) {
            this.varianceKey = value;
        }

        /**
         * Gets the value of the classType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getClassType() {
            return classType;
        }

        /**
         * Sets the value of the classType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setClassType(String value) {
            this.classType = value;
        }

    }

}

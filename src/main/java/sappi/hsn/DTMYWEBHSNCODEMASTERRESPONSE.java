
package sappi.hsn;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * HSN code master response
 * 
 * <p>Java class for DT_MYWEB_HSN_CODE_MASTER_RESPONSE complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DT_MYWEB_HSN_CODE_MASTER_RESPONSE"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="HSN_CODE" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="J_1ICHID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                   &lt;element name="J_1ICHT1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DT_MYWEB_HSN_CODE_MASTER_RESPONSE", propOrder = {
    "hsncode"
})
public class DTMYWEBHSNCODEMASTERRESPONSE {

    @XmlElement(name = "HSN_CODE")
    protected List<DTMYWEBHSNCODEMASTERRESPONSE.HSNCODE> hsncode;

    /**
     * Gets the value of the hsncode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hsncode property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHSNCODE().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DTMYWEBHSNCODEMASTERRESPONSE.HSNCODE }
     * 
     * 
     */
    public List<DTMYWEBHSNCODEMASTERRESPONSE.HSNCODE> getHSNCODE() {
        if (hsncode == null) {
            hsncode = new ArrayList<DTMYWEBHSNCODEMASTERRESPONSE.HSNCODE>();
        }
        return this.hsncode;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="J_1ICHID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *         &lt;element name="J_1ICHT1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "j1ICHID",
        "j1ICHT1"
    })
    public static class HSNCODE {

        @XmlElement(name = "J_1ICHID")
        protected String j1ICHID;
        @XmlElement(name = "J_1ICHT1")
        protected String j1ICHT1;

        /**
         * Gets the value of the j1ICHID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getJ1ICHID() {
            return j1ICHID;
        }

        /**
         * Sets the value of the j1ICHID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setJ1ICHID(String value) {
            this.j1ICHID = value;
        }

        /**
         * Gets the value of the j1ICHT1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getJ1ICHT1() {
            return j1ICHT1;
        }

        /**
         * Sets the value of the j1ICHT1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setJ1ICHT1(String value) {
            this.j1ICHT1 = value;
        }

    }

}

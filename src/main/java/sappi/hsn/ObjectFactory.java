
package sappi.hsn;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the sappi.hsn package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MTMYWEBHSNCODEMASTERRESPONSE_QNAME = new QName("urn:TorrentPharma.com:WebService/Myweb", "MT_MYWEB_HSN_CODE_MASTER_RESPONSE");
    private final static QName _MTMYWEBHSNCODEMASTER_QNAME = new QName("urn:TorrentPharma.com:WebService/Myweb", "MT_MYWEB_HSN_CODE_MASTER");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: sappi.hsn
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DTMYWEBHSNCODEMASTERRESPONSE }
     * 
     */
    public DTMYWEBHSNCODEMASTERRESPONSE createDTMYWEBHSNCODEMASTERRESPONSE() {
        return new DTMYWEBHSNCODEMASTERRESPONSE();
    }

    /**
     * Create an instance of {@link DTMYWEBHSNCODEMASTER }
     * 
     */
    public DTMYWEBHSNCODEMASTER createDTMYWEBHSNCODEMASTER() {
        return new DTMYWEBHSNCODEMASTER();
    }

    /**
     * Create an instance of {@link DTMYWEBHSNCODEMASTERRESPONSE.HSNCODE }
     * 
     */
    public DTMYWEBHSNCODEMASTERRESPONSE.HSNCODE createDTMYWEBHSNCODEMASTERRESPONSEHSNCODE() {
        return new DTMYWEBHSNCODEMASTERRESPONSE.HSNCODE();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTMYWEBHSNCODEMASTERRESPONSE }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DTMYWEBHSNCODEMASTERRESPONSE }{@code >}
     */
    @XmlElementDecl(namespace = "urn:TorrentPharma.com:WebService/Myweb", name = "MT_MYWEB_HSN_CODE_MASTER_RESPONSE")
    public JAXBElement<DTMYWEBHSNCODEMASTERRESPONSE> createMTMYWEBHSNCODEMASTERRESPONSE(DTMYWEBHSNCODEMASTERRESPONSE value) {
        return new JAXBElement<DTMYWEBHSNCODEMASTERRESPONSE>(_MTMYWEBHSNCODEMASTERRESPONSE_QNAME, DTMYWEBHSNCODEMASTERRESPONSE.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DTMYWEBHSNCODEMASTER }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DTMYWEBHSNCODEMASTER }{@code >}
     */
    @XmlElementDecl(namespace = "urn:TorrentPharma.com:WebService/Myweb", name = "MT_MYWEB_HSN_CODE_MASTER")
    public JAXBElement<DTMYWEBHSNCODEMASTER> createMTMYWEBHSNCODEMASTER(DTMYWEBHSNCODEMASTER value) {
        return new JAXBElement<DTMYWEBHSNCODEMASTER>(_MTMYWEBHSNCODEMASTER_QNAME, DTMYWEBHSNCODEMASTER.class, null, value);
    }

}

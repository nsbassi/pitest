
package sappi.hsn;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * HSN Code Master
 * 
 * <p>Java class for DT_MYWEB_HSN_CODE_MASTER complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DT_MYWEB_HSN_CODE_MASTER"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="J_1ICHID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="LANGU" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DT_MYWEB_HSN_CODE_MASTER", propOrder = {
    "j1ICHID",
    "langu"
})
public class DTMYWEBHSNCODEMASTER {

    @XmlElement(name = "J_1ICHID")
    protected String j1ICHID;
    @XmlElement(name = "LANGU")
    protected String langu;

    /**
     * Gets the value of the j1ICHID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJ1ICHID() {
        return j1ICHID;
    }

    /**
     * Sets the value of the j1ICHID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJ1ICHID(String value) {
        this.j1ICHID = value;
    }

    /**
     * Gets the value of the langu property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLANGU() {
        return langu;
    }

    /**
     * Sets the value of the langu property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLANGU(String value) {
        this.langu = value;
    }

}

package insight.pitest.handlers

import insight.pitest.client.Constants

import javax.xml.namespace.QName
import javax.xml.soap.SOAPMessage
import javax.xml.ws.handler.MessageContext
import javax.xml.ws.handler.soap.SOAPHandler
import javax.xml.ws.handler.soap.SOAPMessageContext

/**
 * Created by nsb on 13/10/17.
 */
class PIAuthenticationHandler implements SOAPHandler<SOAPMessageContext> {
    @Override
    Set<QName> getHeaders() {
        return null
    }

    @Override
    boolean handleMessage(SOAPMessageContext context) {
        Boolean outboundProperty = context?.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY)
        if (outboundProperty?.booleanValue()) {
            String authToken = System.getProperty(Constants.PI_AUT_PROP)
            if (authToken) {
                println 'Adding authentication token to outbound request.'
                Map<String, List<String>> requestHeaders = context.get(MessageContext.HTTP_REQUEST_HEADERS) ?: [:]
                requestHeaders << [Authorization: ["Basic $authToken"]]
            }else{
                println "Auth token could not be found using system property $Constants.PI_AUT_PROP"
            }
        }
        logToSystemOut(context)
        return true
    }

    @Override
    boolean handleFault(SOAPMessageContext context) {
        return true
    }

    @Override
    void close(MessageContext context) {

    }

    private void logToSystemOut(SOAPMessageContext smc) {
        Boolean outboundProperty = (Boolean)
        smc.get (MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        if (outboundProperty.booleanValue()) {
            System.out.println("\nOutbound message:");
        } else {
            System.out.println("\nInbound message:");
        }

        SOAPMessage message = smc.getMessage();
        try {
            message.writeTo(System.out);
            System.out.println("");   // just to add a newline
        } catch (Exception e) {
            System.out.println("Exception in handler: " + e);
        }
    }
}

package insight.pitest.client

import sappi.hsn.DTMYWEBHSNCODEMASTER
import sappi.hsn.DTMYWEBHSNCODEMASTERRESPONSE.HSNCODE
import sappi.hsn.SIMYWEBHSNMASTERSYNCOUT
import sappi.hsn.SIMYWEBHSNMASTERSYNCOUTService


/**
 * Created by nsb on 13/10/17.
 */
class WSClient {

    static void main(String[] args) {
        CliBuilder cli = new CliBuilder(usage: 'wsclient -u <username> -p <password> [-l <langu>] [-j <j1ICHID>]')
        cli.h(longOpt: 'help', 'usage information', required: false)

        cli.u(longOpt: 'username', 'username for PI', required: true, args: 1)
        cli.p(longOpt: 'password', 'password for PI', required: true, args: 1)

        cli.w(longOpt: 'wsdl', 'location of wsdl', required: false, args: 1)

        cli.l(longOpt: 'langu', 'language code', required: false, args: 1)
        cli.j(longOpt: 'j1ICHID', 'chapter id', required: false, args: 1)

        def options = cli.parse(args)
        if (options) {
            System.setProperty(Constants.PI_AUT_PROP, getToken(options.u, options.p))
            println "Auth token set to ${System.getProperty(Constants.PI_AUT_PROP)}"
            List<HSNCODE> codes = getHSNCodes(options)
            codes?.each {
                println "Code = ${[j1ICHID: it.j1ICHID, j1ICHT1: it.j1ICHT1]}"
            }
        }
    }

    static String getToken(String user, String pass) {
        Base64.encoder.encodeToString("$user:$pass".bytes)
    }

    static List<HSNCODE> getHSNCodes(def options) {
        URL wsdl = options.w ? options.w.toURL() : null
        SIMYWEBHSNMASTERSYNCOUT port
        if (wsdl) {
            println "Building Client using wsdl "
            port = new SIMYWEBHSNMASTERSYNCOUTService(wsdl).HTTPPort
        } else {
            port = new SIMYWEBHSNMASTERSYNCOUTService().HTTPPort
        }
        String langu = options.l ? options.l.toUppserCase() : 'EN'
        if(options.j){
            port.siMYWEBHSNMASTERSYNCOUT(new DTMYWEBHSNCODEMASTER(langu: langu, j1ICHID: options.j))?.HSNCODE
        }else {
            port.siMYWEBHSNMASTERSYNCOUT(new DTMYWEBHSNCODEMASTER(langu: langu))?.HSNCODE
        }
    }
}
